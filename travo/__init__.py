__version__ = "0.2.8dev"

from .gitlab import Forge, GitLab
from .assignment import Assignment
from .course import Course
from .homework import Homework
